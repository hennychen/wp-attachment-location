<?php
/*
Plugin Name: attachment-location
Plugin URI: http://rachelmccollin.com
Description: 媒体库地点插件
Version: 1.0
Author: Rachel McCollin
Author URI: http://rachelmccollin.com
License: GPLv2
*/

//// apply categories to attachments
//function wp_add_categories_to_attachments() {
//    register_taxonomy_for_object_type( 'category', 'attachment' );
//}
//add_action( 'init' , 'wp_add_categories_to_attachments' );
//
//// apply tags to attachments
//function wp_add_tags_to_attachments() {
//    register_taxonomy_for_object_type( 'post_tag', 'attachment' );
//}
//add_action( 'init' , 'wp_add_tags_to_attachments' );

// register new taxonomy which applies to attachments
function wp_add_location_taxonomy() {
    $labels = array(
        'name'              => '位置',
        'singular_name'     => 'Location',
        'search_items'      => '查询位置',
        'all_items'         => '所有位置',
        'parent_item'       => 'Parent Location',
        'parent_item_colon' => 'Parent Location:',
        'edit_item'         => '编辑位置',
        'update_item'       => '更新位置',
        'add_new_item'      => '添加新位置',
        'new_item_name'     => '新的位置名',
        'menu_name'         => '位置',
    );

    $args = array(
        'labels' => $labels,
        'hierarchical' => true,
        'query_var' => 'true',
        'rewrite' => 'true',
        'show_admin_column' => 'true'
    );

    register_taxonomy( 'location', 'attachment', $args );
}
add_action( 'init', 'wp_add_location_taxonomy' );